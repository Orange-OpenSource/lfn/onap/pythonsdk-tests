import logging
import time
from uuid import uuid4
from onapsdk.aai_element import (
    CloudRegion,
    Complex,
    Customer,
    OwningEntity as AaiOwningEntity,
    Relationship,
    Service as AaiService,
    Tenant,
)
from onapsdk.service import Service
from onapsdk.service_instance import ServiceInstantiation, VnfInstantiation
from onapsdk.vendor import Vendor
from onapsdk.vf import Vf
from onapsdk.vid import LineOfBusiness, OwningEntity, Platform, Project
from onapsdk.vsp import Vsp
 
# Configure logging
logger = logging.getLogger("")
logger.setLevel(logging.DEBUG)
fh = logging.StreamHandler()
fh_formatter = logging.Formatter(
    "%(asctime)s %(levelname)s %(lineno)d:%(filename)s(%(process)d) - %(message)s"
)
fh.setFormatter(fh_formatter)
logger.addHandler(fh)
 
# Create required A&AI resources
GLOBAL_CUSTOMER_ID = "generic"
COMPLEX_PHYSICAL_LOCATION_ID = "integration_test_complex"
COMPLEX_DATA_CENTER_CODE = "1234"
 
CLOUD_REGION_ID = "RegionOne"
CLOUD_REGION_OWNER = "morgan"
 
ESR_USERNAME = "onap-master-daily-vnfs-ci"
ESR_PASSWORD = "WlUk3mZXikxhNS7fKpdhlahtMY01CD5t"
ESR_SERVICE_URL = "https://vim.pod4.opnfv.fr:5000"
 
VENDOR_NAME = "mr"
VSP_NAME = "morganVSP"
VSP_FILEPATH = "/home/morgan/Dev/gitlab.com/onap-tests/onap_tests/templates/heat_files/ubuntu16/ubuntu16.zip" 
VF_NAME = "morganVF"
SERVICE_NAME = "morganService"
 
TENANT_ID = "1b12adadc0ef484aa47ab8693bb84827"
TENANT_NAME= "onap-master-daily-vnfs"

Customer.set_proxy({ 'http': 'socks5h://127.0.0.1:8083', 'https': 'socks5h://127.0.0.1:8083'})

customer = Customer.create(GLOBAL_CUSTOMER_ID, GLOBAL_CUSTOMER_ID, "INFRA")

cmplx = Complex.create(
    physical_location_id=COMPLEX_PHYSICAL_LOCATION_ID,
    data_center_code=COMPLEX_DATA_CENTER_CODE,
    name=COMPLEX_PHYSICAL_LOCATION_ID,
)

cloud_region = CloudRegion.create(
    cloud_owner=CLOUD_REGION_OWNER,
    cloud_region_id=CLOUD_REGION_ID,
    orchestration_disabled=False,
    in_maint=False,
    cloud_type="openstack",
    cloud_region_version="morgan_cloud",
)

cloud_region.link_to_complex(cmplx)
cloud_region.add_esr_system_info(
    esr_system_info_id=str(uuid4()),
    user_name=ESR_USERNAME,
    password=ESR_PASSWORD,
    system_type="openstack",
    service_url=ESR_SERVICE_URL,
    cloud_domain="Default",
)

cloud_region.add_tenant(tenant_id=TENANT_ID,tenant_name=TENANT_NAME)
cloud_region.register_to_multicloud()
 
# Onboarding Create SDC resources
vendor = Vendor(name=VENDOR_NAME)
vendor.onboard()
vsp = Vsp(name=VSP_NAME, vendor=vendor, package=open(VSP_FILEPATH, "rb"))
vsp.onboard()
vf = Vf(name=VF_NAME, vsp=vsp)
vf.onboard()
service = Service(name=SERVICE_NAME, resources=[vf])
service.onboard()
 
# Prepare service for instantiation
AaiService.create(service.unique_uuid, service.name)
customer.subscribe_service(service)
service_subscription = None
for service_sub in customer.service_subscriptions:
    if service_sub.service_type == service.name:
        service_subscription = service_sub
if not service_subscription:
    raise ValueError("No service subscription")
tenant = cloud_region.get_tenant(TENANT_ID)
cloud_region.add_availability_zone("brittany", "KVM")
service_subscription.link_to_cloud_region_and_tenant(cloud_region, tenant)
owning_entity = OwningEntity.create("OE-Generic")
try:
    aai_owning_entity = AaiOwningEntity.create(owning_entity.name, str(uuid4()))
except ValueError:
    aai_owning_entity = AaiOwningEntity.get_by_owning_entity_name(owning_entity.name)
project = Project.create("python_onap_sdk_project")
 
# Instantiate service
service_instantiation = ServiceInstantiation.instantiate_so_ala_carte(
    service,
    cloud_region,
    tenant,
    customer,
    aai_owning_entity,
    project,
    service_instance_name="test",
)
while not service_instantiation.finished:
    print("Wait for service instantiation")
    time.sleep(10)
platform = Platform.create("Python_ONAPSDK_Platform")
line_of_business = LineOfBusiness.create("Python_ONAPSDK_LineOfBusiness")
vnf_instantiation = next(
    service_instantiation.instantiate_vnf(line_of_business, platform)
)
while not vnf_instantiation.finished:
    print("Wait for vnf instantiation")
    time.sleep(10)
vf_module_instantiation = next(vnf_instantiation.instantiate_vf_module())
while not vf_module_instantiation.finished:
    print("Wait for vf module instantiation")
    time.sleep(10)
