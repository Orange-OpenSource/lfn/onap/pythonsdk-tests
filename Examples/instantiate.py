#!/usr/bin/python
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=duplicate-code
#  pylint: disable=missing-docstring
#  pylint: disable=too-many-branches
#  pylint: disable=too-many-public-methods
"""Instantiation class."""
import logging
import os
import time
import json

import openstack
from openstack.config import loader

import onap_tests.components.aai as aai
import onap_tests.components.initonapdata as initonapdata
import onap_tests.components.so as so
import onap_tests.components.sdnc as sdnc
import onap_tests.components.nbi as nbi
import onap_tests.utils.openstack_utils as openstack_utils
import onap_tests.utils.utils as onap_utils
import onap_tests.utils.exceptions as onap_test_exceptions

PROXY = onap_utils.get_config("general.proxy")


class Instantiate():
    """
    VNF: Class to automate the instantiation of a VNF.

    It is assumed that the Design phase has been already done
    The yaml template is available and stored in the template directory
    """

    __logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        """Initialize Solution object."""
        super(Instantiate, self).__init__()
        self.vnf_config = {}
        self.net_config = {}
        self.components = {}
        if "service_name" not in kwargs:
            # by convention is VNF is not precised we set ubuntu16
            kwargs["service_name"] = "ubuntu16"
        self.vnf_config["vnf"] = kwargs["service_name"]
        self.vnf_config["subscription_type"] = (
            onap_utils.get_subscription_type(self.vnf_config["vnf"]))
        self.vnf_config["nbi"] = False
        if "nbi" in kwargs:
            self.vnf_config["nbi"] = kwargs["nbi"]

        self.vnf_config["customer"] = "EMPTY"
        if "customer_name" in kwargs:
            self.vnf_config["customer"] = kwargs["customer_name"]

        if "namespace_name" not in kwargs:
            # For instantiation over k8s, default namespace is k8s
            kwargs["namespace_name"] = "k8s"
        self.vnf_config["namespace"] = kwargs["namespace_name"]

        # can be useful to destroy resources, sdnc module name shall be given
        if "sdnc_vnf_name" in kwargs:
            self.vnf_config["sdnc_vnf_name"] = kwargs["sdnc_vnf_name"]
            # Random part = 6 last char of the the vnf name
            self.vnf_config["random_string"] = kwargs["sdnc_vnf_name"][-6:]
        else:
            self.vnf_config["random_string"] = (
                onap_utils.random_string_generator())
            self.vnf_config["sdnc_vnf_name"] = (
                onap_utils.get_config("onap.service.name") + "_" +
                kwargs["service_name"] + "_" +
                self.vnf_config["random_string"])

        network_list = []
        vnf_list = []
        vf_module_list = []
        network_list = list(onap_utils.get_filtered_template_param(
            self.vnf_config["vnf"],
            "topology_template.node_templates",
            "type",
            "org.openecomp.resource.vl"))
        vnf_list = list(onap_utils.get_filtered_template_param(
            self.vnf_config["vnf"],
            "topology_template.node_templates",
            "type",
            "org.openecomp.resource.vf"))
        if vnf_list:
            vf_module_list = list(onap_utils.get_template_param(
                self.vnf_config["vnf"],
                "topology_template.groups"))
        # Class attributes for instance, vnf and module VF
        self.service_infos = {}
        self.network_infos = {'list': network_list}
        self.vnf_infos = {'list': vnf_list}
        self.module_infos = {'list': vf_module_list}

        # retrieve infos from the configuration files
        self.set_service_instance_var()
        self.set_vnf_var()
        self.set_module_var()
        self.set_net_var()
        self.set_onap_components()

    def set_service_instance_var(self):
        """Set service instance variables from the config file."""
        self.vnf_config["vnf_name"] = onap_utils.get_template_param(
            self.vnf_config["vnf"], "metadata.name")
        self.vnf_config["invariant_uuid"] = onap_utils.get_template_param(
            self.vnf_config["vnf"], "metadata.invariantUUID")
        self.vnf_config["uuid"] = onap_utils.get_template_param(
            self.vnf_config["vnf"], "metadata.UUID")

    def set_vnf_var(self):
        """Set vnf variables from the config file."""
        for i, elt in enumerate(self.vnf_infos['list']):
            vnf_config = {}
            self.__logger.info("get VNF %s info", elt)
            vnf_config["vnf_customization_name"] = elt
            vnf_config["vnf_model_name"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.node_templates." +
                vnf_config["vnf_customization_name"] + ".metadata.name")
            vnf_config["vnf_invariant_id"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.node_templates." +
                vnf_config["vnf_customization_name"] +
                ".metadata.invariantUUID")
            vnf_config["vnf_version_id"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.node_templates." +
                vnf_config["vnf_customization_name"] + ".metadata.UUID")
            vnf_config["vnf_version"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.node_templates." +
                vnf_config["vnf_customization_name"] + ".metadata.version")
            vnf_config["vnf_customization_id"] = (
                onap_utils.get_template_param(
                    self.vnf_config["vnf"],
                    "topology_template.node_templates." +
                    vnf_config["vnf_customization_name"] +
                    ".metadata.customizationUUID"))
            vnf_config["vnf_type"] = list(onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.groups"))[i]
            vnf_config["vnf_generic_name"] = (
                self.vnf_config["vnf_name"] + "-service-instance-" +
                self.vnf_config["random_string"])
            vnf_config["vnf_generic_type"] = (
                self.vnf_config["vnf_name"] + "/" +
                vnf_config["vnf_customization_name"])
            self.vnf_config[elt] = vnf_config
            self.vnf_config[elt]["vnf_parameters"] = \
                onap_utils.get_vnf_parameters(self.vnf_config["vnf"],
                                              str(elt))

    def set_net_var(self):
        """Set network variables from the config file."""
        for i, elt in enumerate(self.network_infos['list']):
            net_config = {}
            self.__logger.debug("get Network %s", i)
            self.__logger.info("get Network %s info", elt)
            net_config["net_customization_name"] = elt
            net_config["net_model_name"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.node_templates." +
                net_config["net_customization_name"] + ".metadata.name")
            net_config["net_invariant_id"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.node_templates." +
                net_config["net_customization_name"] +
                ".metadata.invariantUUID")
            net_config["net_version_id"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.node_templates." +
                net_config["net_customization_name"] + ".metadata.UUID")
            net_config["net_version"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.node_templates." +
                net_config["net_customization_name"] + ".metadata.version")
            net_config["net_customization_id"] = (
                onap_utils.get_template_param(
                    self.vnf_config["vnf"],
                    "topology_template.node_templates." +
                    net_config["net_customization_name"] +
                    ".metadata.customizationUUID"))
            net_config["net_type"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.node_templates." +
                net_config["net_customization_name"] + ".metadata.name")
#        net_config["net_type"] = onap_utils.get_template_param(
#            self.vnf_config["vnf"], "topology_template.node_templates." +
#            net_config["net_customization_name"] + ".properties.network_type")
            net_config["net_technology"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.node_templates." +
                net_config["net_customization_name"] +
                ".properties.network_technology")
            net_config["net_role"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.node_templates." +
                net_config["net_customization_name"] +
                ".properties.network_role")
            net_config["net_external"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.node_templates." +
                net_config["net_customization_name"] +
                ".properties.network_assignments.is_external_network")
            net_config["net_provider"] = "false"
            net_config["net_shared"] = "false"
            self.net_config[elt] = net_config
            self.net_config[elt]["net_subnets"] = \
                onap_utils.get_net_parameters(self.vnf_config["vnf"],
                                              str(elt))

    def set_module_var(self):
        """Set module variables from the config file."""
        for elt in self.vnf_infos['list']:
            vf_config = {}

            # we cannot be sure that the modules are in teh same order
            # than the vnf
            vf_index = onap_utils.get_vf_module_index(
                self.module_infos['list'],
                elt)
            vnf_type = list(onap_utils.get_template_param(
                self.vnf_config["vnf"],
                "topology_template.groups"))[vf_index]
            self.__logger.debug("Get Module info for VNF %s", elt)
            vf_config["sdnc_vnf_type"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.groups." +
                vnf_type +
                ".metadata.vfModuleModelName")
            vf_config["vnf_parameters"] = \
                onap_utils.get_vnf_parameters(self.vnf_config["vnf"],
                                              str(elt))
            vf_config["module_invariant_id"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.groups." +
                vnf_type + ".metadata.vfModuleModelInvariantUUID")
            vf_config["module_name_version_id"] = (
                onap_utils.get_template_param(
                    self.vnf_config["vnf"], "topology_template.groups." +
                    vnf_type + ".metadata.vfModuleModelUUID"))
            vf_config["module_customization_id"] = (
                onap_utils.get_template_param(
                    self.vnf_config["vnf"], "topology_template.groups." +
                    vnf_type + ".metadata.vfModuleModelCustomizationUUID"))
            vf_config["module_version"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.groups." +
                vnf_type + ".metadata.vfModuleModelVersion")
            vf_config["module_version"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.groups." +
                vnf_type + ".metadata.vfModuleModelVersion")
            vf_config["module_version_id"] = onap_utils.get_template_param(
                self.vnf_config["vnf"], "topology_template.groups." +
                vnf_type + ".metadata.vfModuleModelUUID")
            self.vnf_config[elt].update(vf_config)

    def set_onap_components(self):
        """Set ONAP component objects."""
        self.components["aai"] = aai.Aai()
        self.components["initonapdata"] = initonapdata.InitOnapData()
        self.components["so"] = so.So()
        self.components["sdnc"] = sdnc.Sdnc()
        self.components["nbi"] = nbi.Nbi(self.vnf_config["vnf"],
                                         self.vnf_config["sdnc_vnf_name"])

    def instantiate(self):
        """Instantiate a Service with ONAP.

        The instantiation consists in the different steps:
          * Create the service instance (SO)
          * preload the Network in the SDNC
          * Create the Network instance (SO)
          * Create the VNF instance (SO)
          * preload the VNF in the SDNC
          * Create the VF module instance (SO)
        """
        instance_info = {"instance_id": ""}
        vnf_info = {"vnf_id": ""}
        net_info = {"net_id": ""}
        module_info = {}
        module_ref = {"instanceId": ""}
        module_ok = False
        check_vnf = False
        self.__logger.info("Start the instantiation of the Service")
        instance_info = self.create_service_instance()
        # as the VNF module is almost immediate
        # and does not require resource creation out of onap
        # we directly check the creation in the Aai
        # it could be improved by adding a check the so request status
        service_ok = self.components["aai"].check_service_instance(
            self.vnf_config["subscription_type"],
            instance_info["instance_id"],
            self.vnf_config["customer"])
        if service_ok:
            # create Network instance(s)
            for elt in self.network_infos['list']:
                try:
                    # preload Net(s) in SDNC
                    self.preload_net(elt)
                    net_info = self.create_net_instance(elt)
                    net_ref = net_info['net_instance']
                    # check that the module has been created in the AAI
#                    self.components["aai"].check_net_instance(net_ref)
                    self.components["aai"].check_net_instance(
                        net_ref["requestReferences"]["instanceId"])
                    # check Net using OpenStack client directly
                    # check_net = self.check_net(
                    #        self.get_net_instance_name(elt))
                except onap_test_exceptions.SoCompletionException:
                    self.__logger.error(
                        "Network not completed")
                except onap_test_exceptions.SoRequestException:
                    self.__logger.error(
                        "Network request completed but KO")
                except onap_test_exceptions.AaiRequestException:
                    self.__logger.warning(
                        "Network created but not seen in AAI")
            # create VNF instance(s)
            for elt in self.vnf_infos['list']:
                vnf_info = self.create_vnf_instance(elt)
                self.__logger.debug("Check vnf %s ", elt)
                self.vnf_infos[elt]['status'] = True
                # as the VNF module is almost immediate
                # and does not require resource creation out of onap
                # we directly check the creation in the Aai
                # it could be improved by adding a check the so request status
                if not self.components["aai"].check_vnf_instance(
                        vnf_info["vnf_id"]):
                    self.vnf_infos[elt]['status'] = False
                else:
                    # preload VNF(s) in SDNC
                    self.preload(instance_info["instance_id"], elt)
                time.sleep(10)

                if self.vnf_infos[elt]['status']:
                    # create VF module(s)
                    try:
                        module_info = self.create_module_instance(
                            instance_info["instance_id"],
                            self.vnf_infos[elt]["vnf_id"], elt)
                        module_ref = module_info['module_instance']
                        # check that the module has been created in the AAI
#                        self.components["aai"].check_module_instance(
#                            self.vnf_infos[elt]["vnf_id"], module_ref)
                        self.components["aai"].check_module_instance(
                            self.vnf_infos[elt]["vnf_id"],
                            module_ref["requestReferences"]["instanceId"])
                        # check VNF using OpenStack client directly
                        check_vnf = self.check_vnf(
                            self.get_module_instance_name(elt))
                    except onap_test_exceptions.SoCompletionException:
                        self.__logger.error(
                            "VF module not completed")
                    except onap_test_exceptions.SoRequestException:
                        self.__logger.error(
                            "VF module request completed but KO")
                    except onap_test_exceptions.AaiRequestException:
                        self.__logger.warning(
                            "VF module created but not seen in AAI")
        return {"status": module_ok,
                "instance_id": instance_info,
                "vnf_info": vnf_info,
                "module_info": module_info,
                "net_info": net_info,
                "check_heat": check_vnf}

    def clean(self, **kwargs):
        """
        Clean VNF from ONAP.

        :param instance_id: The ID of the VNF service instance
        :param vnf_id: The ID of the VNF instance
        :param module_id: The ID of the VF module instance
        """
        if "instance_id" in kwargs:
            instance_id = kwargs.get('instance_id')
        else:
            instance_id = self.service_infos['instance_id']

        for elt in self.vnf_infos['list']:
            if "vnf_info" in kwargs:
                vnf_id = kwargs['vnf_info'][elt]["vnf_id"]
            else:
                vnf_id = self.vnf_infos[elt]["vnf_id"]

            if "module_info" in kwargs:
                module_id = kwargs['module_info'][elt]["module_id"]
            else:
                module_id = (self.module_infos[elt]["module_instance"]
                             ["requestReferences"]["instanceId"])
            self.clean_module(instance_id, vnf_id, module_id, elt)
            if not self.components["aai"].check_module_cleaned(vnf_id,
                                                               module_id):
                return False

            self.clean_vnf(instance_id, vnf_id, elt)
            if not self.components["aai"].check_vnf_cleaned(vnf_id):
                return False

            self.clean_preload(elt)

        for elt in self.network_infos['list']:
            if "net_info" in kwargs:
                net_id = kwargs['net_info'][elt]["net_instance"]
            else:
                net_id = (self.network_infos[elt]["net_instance"]
                          ["requestReferences"]["instanceId"])
            self.clean_net(instance_id, net_id, elt)
            if not self.components["aai"].check_net_cleaned(net_id):
                return False

        self.clean_instance(instance_id)
        if self.components["aai"].check_service_instance_cleaned(
                self.vnf_config["vnf_name"], instance_id,
                self.vnf_config["customer"]):
            self.__logger.debug("Instance still in AAI DB")
        else:
            return False
        return True

    def create_service_instance(self):
        """
        Create service instance.

        2 options to create the instance
          * with SO
          * with NBI
        """
        instance_id = None
        if self.vnf_config["nbi"]:
            self.__logger.info("1) Create Service instance from NBI")
            nbi_info = self.components["nbi"].create_service_order_nbi()
            self.__logger.debug("NBI infos: %s", nbi_info)
            time.sleep(5)
            instance_id = (
                self.components["nbi"].get_instance_id_from_order(
                    nbi_info["id"]))
        else:
            self.__logger.info("1) Create Service instance in SO")
            service_payload = self.get_service_payload()
            instance_id = self.components["so"].create_instance(
                service_payload)
        service_model_version = onap_utils.get_service_custom_config(
            self.vnf_config["vnf"], "version")
        service_instance_info = {"instance_id": instance_id,
                                 "model_version": service_model_version}
        self.__logger.debug("Service instance created: %s",
                            service_instance_info)
        self.service_infos = service_instance_info
        return service_instance_info

    def create_net_instance(self, elt):
        """
        Create Network instance.

        :param elt: the Network
        """
        self.__logger.info("2.2) Create Network instance %s in SO", elt)

        net_payload = self.get_net_payload(
            self.service_infos["instance_id"], elt)
        net_instance = self.components["so"].create_net(
            self.service_infos["instance_id"],
            net_payload)

        req_ref = net_instance["requestReferences"]["requestId"]
        # check the request status
        state, response = self.components["so"].check_so_request_completed(req_ref)
        if state:
            self.__logger.info("Response: %s", response["request"]
                               ["instanceReferences"]
                               ["networkInstanceId"])
            net_instance["requestReferences"]["instanceId"] = (response["request"]
                                                               ["instanceReferences"]
                                                               ["networkInstanceId"])
            #net_instance = response["request"]["instanceReferences"]["networkInstanceId"]
            self.__logger.debug(
                "Network instance created: %s", net_instance)
            net_info = (
                {'net_instance': net_instance,
                 'net_payload': net_payload})
            self.__logger.info("SO Network created")
            self.__logger.debug("Network details: %s", net_info)
        else:
            self.__logger.error("SO Network not completed")
            net_info = None
        self.network_infos[elt] = net_info
        return net_info

    def create_vnf_instance(self, elt):
        """
        Create VNF instance.

        :param elt: the VNF
        """
        vnf_id = None
        self.__logger.info("3) Create VNF instance %s in SO", elt)

        vnf_payload = self.get_vnf_payload(
            self.service_infos["instance_id"], elt)

        vnf_id = self.components["so"].create_vnf(
            self.service_infos["instance_id"],
            vnf_payload)
        vnf_info = {"vnf_id": vnf_id,
                    "vnf_payload": vnf_payload}
        self.__logger.debug("SO vnf instance created %s", vnf_info)
        self.vnf_infos[elt] = vnf_info
        return vnf_info

    def preload_net(self, elt):
        """
        Preload Network in SDNC.

        :param elt: the Network
        """
        net_preload_infos = {}
        self.__logger.info("2.1) Preload Network %s in SDNC", elt)

        sdnc_payload = self.get_net_preload_payload(elt)
        self.__logger.debug("SDNC preload payload %s", sdnc_payload)
        sdnc_preload = self.components["sdnc"].preload_net(sdnc_payload)
        self.__logger.debug("SDNC preload answer: %s", sdnc_preload)
        net_preload_infos[elt] = ({"sdnc_payload": sdnc_payload,
                                   "sdnc_preload": sdnc_preload})

        return net_preload_infos[elt]

    def preload(self, instance_id, elt):
        """
        Preload VNF in SDNC.

        :param elt: the VNF
        """
        vnf_preload_infos = {}
        self.__logger.info("4.1) Preload VNF %s in SDNC", elt)

        sdnc_payload = self.get_sdnc_preload_payload(instance_id, elt)
        self.__logger.debug("SDNC preload payload %s", sdnc_payload)
        sdnc_preload = self.components["sdnc"].preload(sdnc_payload)
        self.__logger.debug("SDNC preload answer: %s", sdnc_preload)
        vnf_preload_infos[elt] = ({"sdnc_payload": sdnc_payload,
                                   "sdnc_preload": sdnc_preload})

        return vnf_preload_infos[elt]

    def create_module_instance(self, instance_id, vnf_id, elt):
        """
        Create module instance.

        :param instance_info: dict including the instance_id, the request_info
          and the service payload
        :param vnf_info: dict including the vnf_id, vnf_related_instance and
          the vnf payload
        """
        model_info = {}
        module_info = {}
        self.__logger.info("4.2) Create MODULE %s instance in SO", elt)

        module_payload = self.get_module_payload(instance_id, vnf_id, elt)

        model_info = json.loads(module_payload)['requestDetails']['modelInfo']

        self.__logger.debug("Module payload - modelInfo %s", model_info)

        if "k8s" in model_info["modelName"]:
            # only for k8s instantiation: create RbDefinition if needed
            if not self.components["initonapdata"].\
            check_rbdefinition_exists(model_info):
                rb_status = False
                if self.components["initonapdata"].\
                create_rbdefinition_metadata(model_info):
                    rb_status = True
                else:
                    self.__logger.error("k8s - creating rbDef")
                    return rb_status


            # only for k8s instantiation: link RbDefinition to Profile
            if not self.components["initonapdata"].\
            check_k8s_profile_exists(model_info):
                link_status = False
                if self.components["initonapdata"].\
                create_k8s_profile_metadata(model_info, \
                self.vnf_config["namespace"]) \
                and \
                self.components["initonapdata"].\
                upload_k8s_profile_content(model_info):
                    link_status = True
                else:
                    self.__logger.error("k8s - link rbDef to Profile")
                    return link_status

        module_instance = self.components["so"].create_module(
            self.service_infos["instance_id"],
            self.vnf_infos[elt]["vnf_id"],
            module_payload)

        req_ref = module_instance["requestReferences"]["requestId"]
        state, response = self.components["so"].check_so_request_completed(req_ref)
        if state:
            self.__logger.info("Response: %s", response["request"]
                               ["instanceReferences"]
                               ["vfModuleInstanceId"])
            self.__logger.debug(
                "Module instance created: %s", module_instance)
            module_info = (
                {'module_instance': module_instance,
                 'module_payload': module_payload})
            self.__logger.info("SO module vf(s) created")
            self.__logger.debug("VF module(s) details: %s", module_info)
        else:
            self.__logger.error("SO module vf(s) not completed")
            module_info = None
        self.module_infos[elt] = module_info
        return module_info

    def check_vnf(self, stack_name):
        """
        Check VNF stack has been properly started.

        :param stack_name: the OpenStack name of the stack
        """
        check_vnf = False
        if onap_utils.get_config("openstack.check"):
            self.__logger.debug("Openstack check enabled")
            test_cloud = os.getenv('OS_TEST_CLOUD', 'openlab-vnfs-ci')
            loader.OpenStackConfig()
            cloud = openstack.connect(cloud=test_cloud)
            if openstack_utils.check_stack_is_complete(cloud, stack_name):
                check_vnf = True
        else:
            self.__logger.debug("Openstack check disabled")
        return check_vnf

    def clean_instance(self, instance_id):
        """
        Clean VNF instance.

        :param instance_id: The service instance of the VNF
        """
        self.__logger.info(" Clean Service Instance ")
        service_payload = self.get_service_payload()
        self.components["so"].delete_instance(instance_id, service_payload)

    def clean_net(self, instance_id, net_id, elt):
        """
        Clean the network.

        :param instance_id: The service instance
        :param net_id:The NET id of the Network
        """
        self.__logger.info(" Clean net Instance %s ", elt)
        net_payload = self.get_net_payload(instance_id, elt)
        self.components["so"].delete_net(
            instance_id,
            net_id,
            net_payload)

    def clean_vnf(self, instance_id, vnf_id, elt):
        """
        Clean  the VNF.

        :param instance_id: The service instance of the VNF
        :param vnf_id:The VNF id of the VNF
        """
        self.__logger.info(" Clean vnf Instance %s ", elt)
        vnf_payload = self.get_vnf_payload(instance_id, elt)
        self.components["so"].delete_vnf(
            instance_id,
            vnf_id,
            vnf_payload)

    def clean_module(self, instance_id, vnf_id, module_id, elt):
        """
        Clean the VNF Module(s).

        :param instance_id: The service instance id of the VNF
        :param vnf_id:The VNF id of the VNF
        :param module_id: the VF module id of the VNF
        :param elt: the list of modules to be cleaned
        """
        self.__logger.info(" Clean Module VF Instance %s ", elt)
        module_payload = self.get_module_payload(instance_id, vnf_id, elt)
        self.components["so"].delete_module(
            module_payload,
            instance_id,
            vnf_id,
            module_id)

    def clean_preload(self, elt):
        """
        Clean VNF SDNC preload.

        :param elt: the list of VNFs to be cleaned
        """
        self.__logger.info(" Clean Preload of %s ", elt)
        module_instance_name = self.get_module_instance_name(elt)
        # if 1 of the expected preload clean is FAIL we return False
        clean_preload = self.components["sdnc"].delete_preload(
            module_instance_name,
            self.vnf_config[elt]["sdnc_vnf_type"])
        return clean_preload

    def get_service_payload(self):
        """Get Service payload."""
        model_info = self.components["so"].get_service_model_info(
            self.vnf_config['invariant_uuid'], self.vnf_config['uuid'],
            self.vnf_config['customer'])
        request_info = self.components["so"].get_request_info(
            self.vnf_config["vnf"] + "-service-instance-" +
            self.vnf_config['random_string'])
        return self.components["so"].get_service_payload(
            self.vnf_config["subscription_type"],
            request_info,
            model_info,
            self.vnf_config["customer"])

    def get_net_payload(self, instance_id, elt):
        """Get network payload."""
        model_info = self.components["so"].get_net_model_info(
            self.net_config[elt]['net_invariant_id'],
            self.net_config[elt]['net_version_id'],
            self.net_config[elt]['net_version'],
            self.net_config[elt]['net_model_name'],
            self.net_config[elt]['net_customization_id'],
            self.net_config[elt]['net_customization_name'])

        service_model_version = onap_utils.get_service_custom_config(
            self.vnf_config["vnf"], "version")

        net_related_instance = self.components["so"].get_vnf_related_instance(
            instance_id,
            self.vnf_config['invariant_uuid'],
            self.vnf_config['uuid'],
            service_model_version)

        net_instance_name = (self.vnf_config["vnf"] + "-net-instance-" +
                             str(elt).replace(" ", "_") + ("_") +
                             self.vnf_config['random_string'])

        request_info = self.components["so"].get_request_info(
            net_instance_name)

        request_info["productFamilyId"] = self.vnf_config['invariant_uuid']

        return self.components["so"].get_vnf_payload(
            self.vnf_config["vnf"],
            request_info,
            model_info,
            net_related_instance)

    def get_vnf_payload(self, instance_id, elt):
        """
        Get VNF payload.

        :param instance_id: the ONAP instance id
        :param elt: the list of VNFs
        """
        model_info = self.components["so"].get_vnf_model_info(
            self.vnf_config[elt]['vnf_invariant_id'],
            self.vnf_config[elt]['vnf_version_id'],
            self.vnf_config[elt]['vnf_version'],
            self.vnf_config[elt]['vnf_model_name'],
            self.vnf_config[elt]['vnf_customization_id'],
            self.vnf_config[elt]['vnf_customization_name'])

        service_model_version = onap_utils.get_service_custom_config(
            self.vnf_config["vnf"], "version")

        vnf_related_instance = self.components["so"].get_vnf_related_instance(
            instance_id,
            self.vnf_config['invariant_uuid'],
            self.vnf_config['uuid'],
            service_model_version)

        vnf_instance_name = (self.vnf_config["vnf"] + "-vnf-instance-" +
                             str(elt).replace(" ", "_") + ("_") +
                             self.vnf_config['random_string'])

        request_info = self.components["so"].get_request_info(
            vnf_instance_name)

        return self.components["so"].get_vnf_payload(
            self.vnf_config["vnf"],
            request_info,
            model_info,
            vnf_related_instance)

    def get_sdnc_preload_payload(self, instance_id, elt):
        """
        Get SDNC preload payload.

        :param instance_id: the ONAP instance id
        :param elt: the list of VNFs
        """
        vnf_name = (self.vnf_config["vnf"] +
                    "-vfmodule-instance-" +
                    str(elt).replace(" ", "_") + "_" +
                    self.vnf_config['random_string'])

        vnf_instance_name = (self.vnf_config["vnf"] + "-vnf-instance-" +
                             str(elt).replace(" ", "_") + ("_") +
                             self.vnf_config['random_string'])

        vnf_topology_identifier = {
            "generic-vnf-name": vnf_instance_name,
            "generic-vnf-type": (
                self.vnf_config[elt]['vnf_generic_type']),
            "service-type": instance_id,
            "vnf-name": vnf_name,
            "vnf-type": self.vnf_config[elt]['sdnc_vnf_type']}
        return self.components["sdnc"].get_preload_payload(
            self.vnf_config[elt]['vnf_parameters'],
            vnf_topology_identifier)

    def get_net_preload_payload(self, elt):
        """
        Get network preload patylaod.

        :param elt: the list of VNFs
        """
        net_name = (self.vnf_config["vnf"] +
                    "-net-instance-" +
                    str(elt).replace(" ", "_") + "_" +
                    self.vnf_config['random_string'])

        net_topology_identifier = {
            "network-name": net_name,
            "network-type": self.net_config[elt]['net_type'],
            "service-type": self.vnf_config['subscription_type'],
            "network-technology": self.net_config[elt]['net_technology'],
            "network-role": self.net_config[elt]['net_role']}

        net_provider_info = {
            "is-external-network": self.net_config[elt]['net_external'],
            "is-provider-network": self.net_config[elt]['net_provider'],
            "is-shared-network": self.net_config[elt]['net_shared']}

        net_subnet = self.net_config[elt]['net_subnets']

        return self.components["sdnc"].get_net_preload_payload(
            net_topology_identifier, net_provider_info, net_subnet)

    def get_module_payload(self, instance_id, vnf_id, elt):
        """Get module payload."""
        module_model_info = self.components["so"].get_module_model_info(
            self.vnf_config[elt]['module_invariant_id'],
            self.vnf_config[elt]['sdnc_vnf_type'],
            self.vnf_config[elt]['module_customization_id'],
            self.vnf_config[elt]['module_version_id'],
            self.vnf_config[elt]['module_version'])
        module_related_instance = (
            self.components["so"].get_module_related_instance(
                vnf_id,
                self.vnf_config[elt]['vnf_invariant_id'],
                self.vnf_config[elt]['vnf_version_id'],
                self.vnf_config[elt]['vnf_version'],
                self.vnf_config[elt]['vnf_model_name'],
                self.vnf_config[elt]['vnf_customization_id'],
                self.vnf_config[elt]['vnf_customization_name']))

        module_instance_name = self.get_module_instance_name(elt)

        request_info = self.components["so"].get_request_info(
            module_instance_name)

        vnf_instance_name = (self.vnf_config["vnf"] + "-vnf-instance-" +
                             str(elt).replace(" ", "_") + ("_") +
                             self.vnf_config['random_string'])

        vnf_related_instance = self.components["so"].get_vnf_related_instance(
            instance_id,
            self.vnf_config['invariant_uuid'],
            self.vnf_config['uuid'],
            vnf_instance_name)

        return self.components["so"].get_module_payload(
            self.vnf_config["vnf"],
            request_info,
            module_model_info,
            vnf_related_instance,
            module_related_instance)

    def get_module_instance_name(self, elt):
        """
        Get module instance name.

        :param elt: the list of modules
        """
        return (self.vnf_config["vnf"] + "-vfmodule-instance-" +
                str(elt).replace(" ", "_") + "_" +
                self.vnf_config['random_string'])
